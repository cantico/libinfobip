; <?php/*

[general]
name							="LibInfobip"
version							="1.0.3"
encoding						="UTF-8"
description						="Library to connect to Infobip SMS provider"
description.fr					="Bibliothèque de connexion au fournisseur de service de SMS Infobip"
delete							=1
ov_version						="8.2.0"
php_version						="5.4.0"
addon_access_control			=0
author							="Cantico"
icon							="icon.png"
mysql_character_set_database	="latin1,utf8"
configuration_page				="admin"
mod_curl						="Available"

[functionalities]
Mailing						="Available"
;*/?>