<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';


class Func_Infobip extends bab_functionality
{
    private $senderapi = null;
    private $retrieverapi = null;

    public function getDescription()
    {
        return 'Infobip';
    }


    /**
     * @return bool
     */
    public function isConfigured()
    {
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/LibInfobip/');
        $I = $registry->getValueEx(array('infobip_disable', 'infobip_user', 'infobip_password', 'infobip_number'));
        foreach($I as $arr)
        {
            ${$arr['key']} = $arr['value'];
        }

        if($infobip_disable || !$infobip_user || !$infobip_password || !$infobip_number) {
            return false;
        }

        return true;
    }


    private function includeFiles()
    {
        require_once dirname(__FILE__).'/api/autoload.php';
    }


    /**
     *
     * @throws LibMailingMailjetException
     * @return infobip\api\client\SendMultipleTextualSmsAdvanced
     */
    private function senderAPI()
    {
        if (null === $this->senderapi)
        {
            if (!class_exists('infobip\\api\\client\\SendMultipleTextualSmsAdvanced')) {
                $this->includeFiles();
            }

            $registry = bab_getRegistryInstance();
            $registry->changeDirectory('/LibInfobip/');

            $I = $registry->getValueEx(array('infobip_user', 'infobip_password', 'infobip_number'));

            foreach($I as $arr)
            {
                ${$arr['key']} = $arr['value'];
            }

            if ((!isset($infobip_user)) || (!isset($infobip_password)))
            {
                throw new LibInfobipException('missing Infobip configuration');
            }

            //$this->infobip_user = $infobip_user;
            //$this->infobip_password = $infobip_password;
            $this->infobip_number = $infobip_number;
            $this->senderapi = new infobip\api\client\SendMultipleTextualSmsAdvanced(new infobip\api\configuration\BasicAuthConfiguration($infobip_user, $infobip_password));
        }

        return $this->senderapi;
    }


    /**
     *
     * @throws LibMailingMailjetException
     * @return infobip\api\client\GetReceivedMessages
     */
    private function retrieverAPI()
    {
        if (null === $this->retrieverapi)
        {
            if (!class_exists('infobip\\api\\model\\sms\\mt\\send\\textual\\SMSTextualRequest')) {
                $this->includeFiles();
            }

            $registry = bab_getRegistryInstance();
            $registry->changeDirectory('/LibInfobip/');

            $I = $registry->getValueEx(array('infobip_user', 'infobip_password', 'infobip_number'));

            foreach($I as $arr)
            {
                ${$arr['key']} = $arr['value'];
            }

            if ((!isset($infobip_user)) || (!isset($infobip_password)))
            {
                throw new LibInfobipException('missing Infobip configuration');
            }

            //$this->infobip_user = $infobip_user;
            //$this->infobip_password = $infobip_password;
            $this->infobip_number = $infobip_number;
            $this->retrieverapi = new infobip\api\client\GetReceivedMessages(new infobip\api\configuration\BasicAuthConfiguration($infobip_user, $infobip_password));
        }

        return $this->retrieverapi;
    }

    /**
     * Convert a standart french number to a international one
     *
     * @param string $number
     *
     * @return string
     */
    protected function checkNumber($number)
    {
        $number = str_replace(' ', '', $number);
        if(substr($number, 0, 1) === '0') {
            $number = substr($number, 1);
        }
        if(substr($number, 0, 1) != '+') {
            $number = '+33'.$number;
        }
        return $number;
    }

    /**
     * @param stirng 	$to 		destination phone number
     * @param stirng 	$msg		SMS text message
     * @param string	$callback	The url where to deliver the sending report
     */
    public function Send($to, $msg, $callback = false)
    {
        $API = $this->senderAPI();

        $destination = new infobip\api\model\Destination();
        $to = $this->checkNumber($to);
        $destination->setTo($to);

        $message = new infobip\api\model\sms\mt\send\Message();
        $message->setFrom($this->infobip_number);
        $message->setDestinations([$destination]);
        $message->setText($msg);
        $message->setTransliteration("NON_UNICODE");
        if ($callback) {
            $message->setNotifyUrl($callback);
        }

        $requestBody = new infobip\api\model\sms\mt\send\textual\SMSAdvancedTextualRequest();
        $requestBody->setMessages([$message]);

        /*$mapper = new JsonMapper();
        $responseObject = $mapper->map(json_decode($responseBody), new infobip\api\model\sms\mt\reports\SMSReportResponse());

        return $responseObject->getResults();*/


        $response = $API->execute($requestBody);
        
        foreach ($response->getMessages() as $respMessage) {
            if (1 !== $respMessage->getSmsCount()) {
                trigger_error('SMS content too long: '.$msg);
            }
        }
        
        return $response;
    }

    /**
     *
     */
    public function Retrieve()
    {
        $API = $this->retrieverAPI();

        $context = new infobip\api\model\sms\mo\reports\GetReceivedMessagesExecuteContext;
        //$context->

        $response = $API->execute($context);

        $return = array();
        for ($i = 0; $i < count($response->getResults()); ++$i) {
            $result = $response->getResults()[$i];
            $return[] = array(
                'id' => $result->getMessageId(),
                'receivedDate' => $result->getReceivedAt()->format('y-M-d H:m:s'),
                'senderBy' => $result->getFrom(),
                'receiveBy' => $result->getTo(),
                'message' => $result->getText(),
                'tag' => $result->getKeyword(),
                'clean_message' => $result->getCleanText(),
                'sms_count' => $result->getSmsCount()
            );
        }

        return $return;
    }

    /**
     * @param stirng 	$input 		destination phone number
     */
    public function ParseCallback($input)
    {
        if (!class_exists('infobip\\api\\model\\sms\\mt\\send\\textual\\SMSTextualRequest')) {
            $this->includeFiles();
        }

        $mapper = new JsonMapper();
        $responseObject = $mapper->map(json_decode($input), new infobip\api\model\sms\mt\reports\SMSReportResponse());

        $return = array();
        for ($i = 0; $i < count($responseObject->getResults()); ++$i) {
            $result = $responseObject->getResults()[$i];
            $return[] = array(
                'id' => $result->getMessageId(),
                'receivedDate' => $result->getReceivedAt()->format('y-M-d H:m:s'),
                'senderBy' => $result->getFrom(),
                'receiveBy' => $result->getTo(),
                'price' => $result->getPrice()->getPricePerMessage(),
                'currency' => $result->getPrice()->getCurrency(),
                'status' => $result->getStatus()->getName()
            );
        }

        return $return;
    }

    /**
     * @param stirng 	$input 		destination phone number
     */
    public function ParseReceive($input)
    {
        if (!class_exists('infobip\\api\\model\\sms\\mt\\send\\textual\\SMSTextualRequest')) {
            $this->includeFiles();
        }
        $mapper = new JsonMapper();
        $responseObject = $mapper->map(json_decode($input), new infobip\api\model\sms\mo\reports\MOReportResponse());

        $result = $responseObject->getResults()[0];
        return array(
            'id' => $result->getMessageId(),
            'receivedDate' => $result->getReceivedAt()->format('y-M-d H:m:s'),
            'senderBy' => $result->getFrom(),
            'receiveBy' => $result->getTo(),
            'message' => $result->getText(),
            'tag' => $result->getKeyword(),
            'clean_message' => $result->getCleanText(),
            'sms_count' => $result->getSmsCount()
        );
    }

}


/**
 *
 *
 */
class LibInfobipException extends Exception {

}